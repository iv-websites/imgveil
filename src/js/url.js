// Search query
var search = new URLSearchParams(window.location.search);
var img = search.get("img");

// JSON
const json = "/src/img/img.json";


// Gets input and goes to link
function getSearch() {
    var query = $("input").val();

    // Validation
    if (query != "") {
        document.location.replace("?img=" + query);
    };
};


// Loads JSON file
$.getJSON(json, function(data) {

    // Length
    const imgLength = Object.keys(data).length;
    var found = false;

    // Checks each entry
    for (i=0;i<imgLength;i++) {

        // Checking if there was an image specified
        if (img == null) {
            break;
        };

        // Correct image
        if (data[i]["name"] == img) {

            // Set data variables
            var filetype = data[i]["filetype"];
            var imgTitle = data[i]["title"];
            var imgDesc = data[i]["desc"];
            var imgDate = data[i]["date"];

            // Exit loop
            found = true;
            break;
        };
    };

    // 404 - image not found
    if(!found) {

        // Hide image
        $(".image").addClass("hidden");

        // Show homepage
        $(".title").removeClass("hidden");
        $(".welcome").removeClass("hidden");
        $(".search").removeClass("hidden");

        // If there actually was no image and the box wasn't empty
        if(img != null) {
            $("#pageTitle").html(img + " does not exist")
        };
    
    // Image found
    } else {

        // Show image
        $(".image").removeClass("hidden");

        // Hide homepage
        $(".title").addClass("hidden");
        $(".welcome").addClass("hidden");
        $(".search").addClass("hidden");

        // Show image information
        $("#img").attr("src", "/src/img/" + img + filetype);
        $(".imgTitle").html(imgTitle);
        $(".imgDesc").html(imgDesc);
        $(".imgDate").html(imgDate);
    };

    
    // Search button
    $("button").on("click", function() {
        getSearch();
    });
})